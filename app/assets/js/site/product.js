$("#create-product").submit(function(){
	createProduct()
	return false
})

$("#update-product").submit(function(){
	updateProduct()
	return false
})

$(".action.edit").click(function(){
	window.location = this.dataset.url
})

$(".action.delete").click(function(){
	if (confirm("Tem certeza que deseja deletar o produto ?")){
		deleteProduct(this.dataset.id)
	}
	return false
})

function createProduct(){
	const image = $("#image").get(0).files[0]
	let formData = new FormData()
	const data = {
		sku: $("#sku").val(),
		name: $("#name").val(),
		price: $("#price").val(),
		quantity: $("#quantity").val(),
		categories: $("#categories").val(),
		description: $("#description").val()
	}

	formData.append('image', image)
	formData.append('data', JSON.stringify(data))
	validationErrors = validateAttributes(data)
	if (validationErrors.length === 0){
		$.ajax({
			url: "http://localhost/assessment-backend/products/create",
			data: formData,
			method:"POST",
			contentType: false,
			processData: false,
			success: function (response){

				let errorElement = $("<p></p>", {
					text: "Produto Adicionado com sucesso",
					class: 'success'
				}).appendTo("#create-product")
			}
		})
		$("#sku").val("")
		$("#name").val("")
		$("#price").val("")
		$("#quantity").val("")
		$("#categories").val("")
		$("#description").val("")
		$("#image").val("")
	}

	showErrors(validationErrors)

}

function deleteProduct(id){
	const data = {id: id}
	$.ajax({
		url: "http://localhost/assessment-backend/products/delete",
		data: data,
		method:"DELETE",
		success: function (response){
			let errorElement = $("<p></p>", {
				text: "Produto Deletado com sucesso",
				class: 'success'
			}).prependTo(".data-grid")
			setTimeout(function(){
				location.reload()
			}, 500)
		}
	})
}
function updateProduct(){
	const data = {
		id: $("#id").val(),
		sku: $("#sku").val(),
		name: $("#name").val(),
		price: $("#price").val(),
		quantity: $("#quantity").val(),
		categories: $("#categories").val(),
		description: $("#description").val()
	}

	validationErrors = validateAttributes(data)
	if (validationErrors.length === 0){
		$.ajax({
			url: `http://localhost/assessment-backend/products/updateAction`,
			data: data,
			method: "PUT",
			success: function (response){
				let errorElement = $("<p></p>", {
					text: "Produto Atualizado com sucesso",
					class: 'success'
				}).appendTo("#update-product")
				setTimeout(function(){
					location.reload()
				}, 1000)
			}
		})
	}

	showErrors(validationErrors)

}

function showErrors(errors){

	errors.forEach(function(e){
		let errorElement = $("<p></p>", {
			text: e,
			class: 'error'
		}).appendTo("#create-product")
	})

}


function validateAttributes(data){
	let errors = []
	if (data.sku === null || data.sku.length === 0){
		errors.push("Sku deve ser preenchido")
	}

	if (data.name === null || data.name.length === 0){
		errors.push("Nome do produto deve ser preenchido")
	}

	if (data.price === null || data.price.length === 0){
		errors.push("Preço deve ser preenchido")
	}

	if (data.quantity === null || data.quantity.length === 0){
		errors.push("Quantidade deve ser preenchido")
	}

	if (data.categories.length === 0){
		errors.push("É necessário selecionar ao menos uma categoria")
	}

	if (data.description === null || data.description.length === 0){
		errors.push("Informe uma descrição para o produto")
	}

	return errors
}