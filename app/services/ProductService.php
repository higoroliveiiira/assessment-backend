<?php

namespace App\services;

use App\config\Database;
use App\models\Product;

class ProductService implements IService
{

    private $__dbConnection = NULL;

    public function __construct()
    {
        $this->__dbConnection = Database::getConnection();
    }

    public function getList()
    {
        $productsInDb = Product::getList($this->__dbConnection);
        $productList = [];

        foreach($productsInDb as $product)
        {   
            array_push($productList, $this->factory($product));
        }

        return $productList;
    }

    public function getListWithLimit($limit)
    {
        $productsInDb = Product::getListWithLimit($this->__dbConnection, $limit);
        $productList = [];

        foreach($productsInDb as $product)
        {   
            array_push($productList, $this->factory($product));
        }

        return $productList;
    }

    public function load($id)
    {
        return $this->factory(Product::loadProduct($this->__dbConnection, $id));
    }

    public function getProductCount()
    {
        return Product::getProductCount($this->__dbConnection);
    }

    public function create($productData)
    {
        $product = $this->factory($productData);
        return Product::createProduct($this->__dbConnection, $product);
    }

    public function update($productData)
    {
        $product = $this->factory($productData);

        return Product::updateProduct($this->__dbConnection, $product);
    }

    public function delete($id)
    {
        return Product::deleteProduct($this->__dbConnection, $id);
    }

    public function factory($productData)
    {
        return new Product($productData);
    }
}
