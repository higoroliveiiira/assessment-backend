<?php

namespace App\controllers;

use App\services\ProductService;

class ProductListController extends BaseController implements IController
{

    public function __construct()
    {
        $this->_service = new ProductService();
    }

    public function process()
    {

    }

	public function index()
	{  
		$this->renderView("products", $this->_service->getList());
	}

}