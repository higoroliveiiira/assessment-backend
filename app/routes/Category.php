<?php 

$array_routes['/assessment-backend/categories/'] = array(         // Route
    "controller" => "App\controllers\CategoryListController", // Controller 
    "action"     => "index"                           // Function 
);

$array_routes['/assessment-backend/categories/new'] = array(         // Route
    "controller" => "App\controllers\CreateCategoryController", // Controller 
    "action"     => "index"                           // Function 
);

$array_routes['/assessment-backend/categories/create'] = array(         // Route
    "controller" => "App\controllers\CreateCategoryController", // Controller 
    "action"     => "process"                           // Function 
);

$array_routes['/assessment-backend/categories/update'] = array(         // Route
    "controller" => "App\controllers\UpdateCategoryController", // Controller 
    "action"     => "index"                           // Function 
);

$array_routes['/assessment-backend/categories/updateAction'] = array(         // Route
    "controller" => "App\controllers\UpdateCategoryController", // Controller 
    "action"     => "process"                           // Function 
);

$array_routes['/assessment-backend/categories/delete'] = array(         // Route
    "controller" => "App\controllers\DeleteCategoryController", // Controller 
    "action"     => "process"                           // Function 
);


return $array_routes;