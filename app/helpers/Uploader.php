<?php

namespace App\helpers;

class Uploader
{
    /**
     * Upload an image.
     * @param $dir  - Image target directory.
     * @param $file - Image to upload.
     */
    public static function doUpload($dir, $file)
    {   

        $targetFile = $dir . basename($file['name']);
        $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));

        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            return false;
        }

        if (move_uploaded_file($file['tmp_name'], $targetFile)) {
            return true;
        }
        
    }
}
