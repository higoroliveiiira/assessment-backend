<?php

namespace App\controllers;

use App\services\ProductService;
use App\services\CategoryService;

class UpdateProductController extends BaseController implements IController
{

    public function __construct()
    {
        $this->_service = new ProductService();
    }

    public function process()
    {   
        $requestData = [];
        parse_str(file_get_contents('php://input'), $requestData);
        $requestData['categories'] = implode(',', $requestData['categories']);
        return $this->_service->update($requestData);
    }


	public function index()
	{  
        $categoryService = new CategoryService();
        $productId = (int) $_GET['id'];
        $params = [
            'categories' => $categoryService->getList(), 
            'product'=> $this->_service->load($productId)
        ];

		$this->renderView("updateProduct", $params);
	}

}