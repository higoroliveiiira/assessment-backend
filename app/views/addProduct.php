
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Add Product</title>
 <?php require_once "header.php"?>
  <main class="content">
    <h1 class="title new-item">New Product</h1>

    <form id="create-product" enctype="multipart/form-data">
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" id="sku" class="input-text" name="sku"/> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" class="input-text" name="name"/> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="number" min="0" max="100000" step="0.01" id="price" class="input-text" name="price"/> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="number" min="0" max="100000" id="quantity" class="input-text" name="qty" /> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="categories" class="input-text" name="categories">
          <?php foreach ($param as $category):?>
          <option value="<?=$category->getId()?>"><?=$category->getName()?></option>
          <?php endforeach;?>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" class="input-text"></textarea>
      </div>
      <div class="input-field">
        <label for="image" class="label">Imagem</label>
       <input type="file" id="image" class="input-text" name="image" /> 
      </div>
      <div class="actions-form">
        <a href="http://localhost/assessment-backend/products/" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
      </div>

    </form>
  </main>
  <?php require_once "footer.php"?>
</body>
</html>
