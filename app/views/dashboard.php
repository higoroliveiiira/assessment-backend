
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Dashboard</title>
  <?php require_once "header.php";?>
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
      You have <?=$param['count']?> products added on this store: <a href="http://localhost/assessment-backend/products/new" class="btn-action">Add new Product</a>
    </div>
    <ul class="product-list">
      <?php foreach($param['products'] as $product):?>
      <li>
        <?php if (!$product->getImage()):?>
        <?php $imageUrl = PATH_URL_IMAGES.'/product/no-image-available-icon-photo-260nw-1251146734.webp' ?>
        <?php else:?>
        <?php $imageUrl = PATH_URL_IMAGES.'/product/'.$product->getImage();?>
        <?php endif;?>
        <div class="product-image">
          <img src="<?= $imageUrl?>" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
        </div>
        <div class="product-info">
          <div class="product-name"><span><?=$product->getName()?></span></div>
          <div class="product-price">
            <span class="special-price"><?=$product->getQuantity()?> available</span>
            <span>R$ <?=$product->getPrice()?></span>
          </div>
        </div>
      </li>
      <?php endforeach;?>
    </ul>
  </main>
  <?php require_once "footer.php"?>
</body>
</html>
