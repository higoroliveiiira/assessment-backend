<?php

namespace App\controllers;

use App\services\ProductService;

class DashboardController extends BaseController implements IController
{

	public function __construct()
    {
        $this->_service = new ProductService();
    }

    public function process()
    {

    }

	public function index()
	{

		$this->renderView(
			"dashboard", 
			['products' => $this->_service->getListWithLimit(4), 'count' => $this->_service->getProductCount()]
		);
	}

}