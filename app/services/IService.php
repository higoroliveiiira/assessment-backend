<?php

namespace App\services;

interface IService
{

    public function getList();
    public function load($id);
    public function create($data);
    public function update($data);
    public function delete($id);
    public function factory($data);
}
