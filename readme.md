# Desenvolvedor PHP Backend Webjump
Teste desenvolvido para aplicação a vaga

# Como rodar o projeto ?
- Clone esse repositório em `/var/www/html/`
- Execute `git checkout desenvolvedor-php-backend` para acessar a branch com o código desenvolvido.
- Importe o arquivo `db.sql` em seu SGBD mysql para a criação do Schema e das tabelas do banco de dados.
- Inicie seu servidor apache caso não esteja rodando.
- Após isso já será possível realizar as operações solicitadas no teste.

# Importação do arquivo import.csv
- Acesse a pasta `assessment-backend`
- Execute o comando `php -f importcsv.php`.
- O arquivo `importcsv.php` é responsável por fazer a importação do arquivo para o banco de dados.

# Imagens
- As imagens importadas na criação dos produtos estão disponíveis na pasta `app/assets/images/product`

# Logs
- Os logs de ação gerados estão disponíveis na pasta em `app/assets/log/acoes.log`

**Estou à disposição para sanar quaisquer dúvidas à respeito do desenvolvimento do projeto**

**Desde já agradeço a oportunidade**