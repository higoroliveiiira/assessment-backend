<?php

namespace App\controllers;

use App\services\ProductService;
use App\services\CategoryService;

class CreateProductController extends BaseController implements IController
{

    public function __construct()
    {
        $this->_service = new ProductService();
    }

    public function process()
    {   
        $requestData = (array) json_decode($_POST['data']);
        $image = isset($_FILES) ? $_FILES['image']: NULL;
        $requestData['image'] = $image;
        $requestData['categories'] = implode(',', $requestData['categories']);
        return $this->_service->create($requestData);
    }

    private function getAvailableCategories()
    {
        $categoryService = new CategoryService();
        return $categoryService->getList();
    }

	public function index()
	{  
		$this->renderView("addProduct", $this->getAvailableCategories());
	}

}