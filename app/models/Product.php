<?php

namespace App\models;

use App\helpers\Uploader;
use App\helpers\Logger;

class Product 
{
    private $__id;
    private $__name;
    private $__sku;
    private $__description;
    private $__price;
    private $__quantity;
    private $__categories;
    private $__image;

    public function __construct($data)
    {
        $this->__id = isset($data['id']) ? $data['id'] : NULL;
        $this->__name = $data['name'];
        $this->__sku = $data['sku'];
        $this->__description = $data['description'];
        $this->__price = (float) $data['price'];
        $this->__quantity = $data['quantity'];
        $this->__categories = isset($data['categories']) ? explode(',', $data['categories']): [];
        $this->__image = isset($data['image']) ? $data['image'] : NULL;
    }

    public function getId()
    {
        return $this->__id;
    }    

    public function getName()
    {
        return $this->__name;
    }    

    public function getSku()
    {
        return $this->__sku;
    }    

    public function getDescription()
    {
        return $this->__description;
    }

    public function getPrice()
    {
        return $this->__price;
    }    


    public function getQuantity()
    {
        return $this->__quantity;
    }

    public function getCategories()
    {
        return $this->__categories;
    }

    public function getImage()
    {
        return $this->__image;
    }

    /**
     * Get Product List.
     * @param $connection  - Database connection.
    **/
    public static function getList(\PDO $connection)
    {

        $statement = $connection->prepare(
            "SELECT
                prod.*,
                GROUP_CONCAT(cat.name) as categories
            FROM
                products as prod
            INNER JOIN category_has_product 
                ON category_has_product.products_id = prod.id
            INNER JOIN category as cat
                ON cat.id = category_has_product.category_id
            GROUP BY prod.id
            ORDER BY prod.id DESC;
            "
        );
        $statement->execute();
        return $statement->fetchAll($connection::FETCH_ASSOC);
    }

    /**
     * Get Product List with limit.
     * @param $connection  - Database connection.
     * @param $limit  - Qty of products to return.
    **/
    public static function getListWithLimit(\PDO $connection, $limit)
    {

        $statement = $connection->prepare(
            "SELECT
                *
            FROM
                products
            ORDER BY id DESC
            LIMIT :limit;
            "
        );
        $statement->bindValue(":limit", $limit, \PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll($connection::FETCH_ASSOC);
    }

    /**
     * Get Product Info.
     * @param $connection  - Database connection.
     * @param $productId  - Product id to retrieve information.
    **/
    public static function loadProduct(\PDO $connection, $productId)
    {
        try{

            $statement = $connection->prepare(
                "SELECT
                    prod.*,
                    GROUP_CONCAT(cat.id) as categories
                FROM
                    products as prod
                INNER JOIN category_has_product 
                    ON category_has_product.products_id = prod.id
                INNER JOIN category as cat
                    ON cat.id = category_has_product.category_id
                WHERE prod.id = :id
                GROUP BY prod.id;
                "
            );
            $statement->bindValue(":id", $productId, \PDO::PARAM_INT);
            $statement->execute();
            $dataToLog = ['id' => $productId];
            Logger::write('Produto de id %id% carregado com sucesso', $dataToLog);
            return $statement->fetch($connection::FETCH_ASSOC);
        }catch(PDOException $Exception){
            throw new $Exception->getMessage();
        }
    }

     /**
     * Insert a product in database.
     * @param $connection  - Database connection.
     * @param $product  - Product to insert.
    **/
    public static function createProduct(\PDO $connection, Product $product)
    {
        try{

            $statement = $connection->prepare(
                "
                INSERT INTO
                    products
                (name, sku, description, price, quantity, image)
                VALUES
                (:name, :sku, :description, :price, :quantity, :image)
                "
            );

            $statement->bindValue(":name", $product->getName());
            $statement->bindValue(":sku", $product->getSku());
            $statement->bindValue(":description", $product->getDescription());
            $statement->bindValue(":price", $product->getPrice());
            $statement->bindValue(":quantity", $product->getQuantity());
            $statement->bindValue(":image", $product->getImage()['name']);

            $statement->execute();
            $productId = $connection->lastInsertId();
            self::__insertProductCategories($connection, $productId, $product->getCategories());
            $dataToLog = ['id' => $productId, 'name' => $product->getName()];
            Logger::write('Produto de codigo %id% e nome %name% criado com sucesso', $dataToLog);
            return Uploader::doUpload(getcwd().'/app/assets/images/product/', $product->getImage());
        }catch(PDOException $Exception){
            throw new $Exception->getMessage();
        }
    }

    /**
     * Update a product in database.
     * @param $connection  - Database connection.
     * @param $product  - Product to update.
    **/
    public static function updateProduct(\PDO $connection, Product $product)
    {
        try{

            $statement = $connection->prepare(
                "
                UPDATE
                    products
                SET 
                    name = :name, 
                    sku = :sku,
                    description = :description,
                    price = :price,
                    quantity = :quantity
                WHERE
                    id = :id
                "
            );


            $statement->bindValue(":name", $product->getName());
            $statement->bindValue(":sku", $product->getSku());
            $statement->bindValue(":description", $product->getDescription());
            $statement->bindValue(":price", $product->getPrice());
            $statement->bindValue(":quantity", $product->getQuantity());
            $statement->bindValue(":id", $product->getId());

            $statement->execute();
            $dataToLog = ['id' => $product->getId(), 'name' => $product->getName()];
            Logger::write('Produto de codigo %id% e nome %name% atualizado com sucesso', $dataToLog);
            self::__deleteProductCategories($connection, $product->getId());
            return self::__insertProductCategories($connection, $product->getId(), $product->getCategories());
        }catch(PDOException $Exception){
            throw new $Exception->getMessage();
        }
    }

    /**
     * Delete a product x category relationship from database.
     * @param $connection  - Database connection.
     * @param $productId  - Product id to delete.
    **/
    private static function __deleteProductCategories(\PDO $connection, $productId)
    {
        $statement = $connection->prepare(
            "
            DELETE FROM
                category_has_product
            WHERE
                products_id = :products_id
            "
        );
        $statement->bindValue(":products_id", $productId);
        $statement->execute();
    }

    /**
     * Insert a product x category relationship in database.
     * @param $connection  - Database connection.
     * @param $productId  - Product id to insert.
    **/
    private static function __insertProductCategories(\PDO $connection, $productId, $categories)
    {
        foreach ($categories as $category)
        {
            $statement = $connection->prepare(
                "
                INSERT INTO
                    category_has_product
                (products_id, category_id)
                VALUES 
                (:products_id, :category_id);
                "
            );
            $statement->bindValue(":products_id", $productId);
            $statement->bindValue(":category_id", $category);
            $statement->execute();
        }

        return "Produto adicionado com sucesso";
    }

    /**
     * Delete a product in database.
     * @param $connection  - Database connection.
     * @param $productId  - Product id to delete.
    **/
    public static function deleteProduct(\PDO $connection, $productId)
    {   
        try{
            $statement = $connection->prepare(
                "
                DELETE FROM
                    products
                WHERE
                    id = :id
                "
            );
            $statement->bindValue(":id", $productId);
            $statement->execute();
            $dataToLog = ['id' => $productId];
            Logger::write('Produto de codigo %id% excluído com sucesso', $dataToLog);
            return "Produto Removido com Sucesso";
        }catch(PDOException $Exception){
            throw new $Exception->getMessage();
        }
    }

    /**
     * Get count of products in database
     * @param $connection  - Database connection.
    **/
    public static function getProductCount(\PDO $connection)
    {
        try{
            $statement = $connection->prepare(
                "
                SELECT
                   count(*)
                FROM
                    products
                "
            );
            $statement->execute();
            return $statement->fetchColumn();
        }catch(PDOException $Exception){
            throw new $Exception->getMessage();
        }
    }

}
