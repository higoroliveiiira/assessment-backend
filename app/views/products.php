
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Products</title>
  <meta charset="utf-8">
  <?php require_once "header.php"?>

  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Products</h1>
      <a href="http://localhost/assessment-backend/products/new" class="btn-action">Add new Product</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
          <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
          <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
          <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
          <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
          <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
          <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php foreach($param as $key => $product):?>
      <tr class="data-row">
        <td class="data-grid-td">
          <span class="data-grid-cell-content"><?=$product->getName()?></span>
        </td>

        <td class="data-grid-td">
          <span class="data-grid-cell-content"><?=$product->getSku()?></span>
        </td>

        <td class="data-grid-td">
          <span class="data-grid-cell-content">R$ <?=$product->getPrice()?></span>
        </td>

        <td class="data-grid-td">
          <span class="data-grid-cell-content"><?=$product->getQuantity()?></span>
        </td>
        <td class="data-grid-td">
        <?php foreach($product->getCategories() as $category): ?>
          <span class="data-grid-cell-content"><?=$category?></span>
          <br />
        <?php endforeach; ?>
        </td>

        <td class="data-grid-td">
          <div class="actions">
            <div class="action edit" data-url="http://localhost/assessment-backend/products/update?id=<?=$product->getId()?>">
              <span>Edit</span>
            </div>
            <div class="action delete" data-id="<?=$product->getId()?>"><span>Delete</span></div>
          </div>
        </td>
      </tr>
      <?php endforeach;?>
    </table>
  </main>


  <?php require_once "footer.php"?>

</body>
</html>
