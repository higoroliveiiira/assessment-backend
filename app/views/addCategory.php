
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Add Category</title>
  <meta charset="utf-8">
  <?php require_once "header.php"?>
  <main class="content">
    <h1 class="title new-item">New Category</h1>
    
    <form id="create-category">
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" id="category-name" class="input-text" />
        
      </div>
      <div class="actions-form">
        <a href="http://localhost/assessment-backend/categories/" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" value="Save" />
      </div>
    </form>
  </main>
  <?php require_once "footer.php"?>
</body>
</html>
