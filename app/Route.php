<?php

class Route 
{
	private $routes;         
	private $https = false; 

	public function initApp()
	{
		$this->initRoutes();
		$this->run($this->getUrl());
	}
	
	private function initRoutes()
	{	
		$dashboard = require_once(__DIR__ . "/routes/Dashboard.php");
		$product = require_once(__DIR__ . "/routes/Product.php");
		$category = require_once(__DIR__ . "/routes/Category.php");
		$array_routes = array_merge($dashboard, $product, $category);
		$this->routes = $array_routes;
	}

	/**
	* Identifies which controller to respond to url
	* @param $url
	*/
	private function run($url)
	{	

		if(!empty($this->routes[$url])){

			$controller = new $this->routes[$url]['controller'];
			$action     = $this->routes[$url]['action'];
			$controller->$action();
		}
		else{
			echo 'ERROR 404 - Page not found';
		}
	}

	/**
	* Get url.
	* @return String url.
	*/
	private function getUrl()
	{
		if($this->https){
			if (isset($_SERVER['HTTPS'])) {
				return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
			}
			else{
				header('location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			}
		}
		else{
			if (isset($_SERVER['HTTPS'])){
				header('location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			}
			else{
				return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
			}
		}
	}
}
