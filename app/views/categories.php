
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Categories</title>

  <?php require_once "header.php"?>
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categories</h1>
      <a href="http://localhost/assessment-backend/categories/new" class="btn-action">Add new Category</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>

      <?php foreach ($param as $category):?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$category->getName()?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$category->getId()?></span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
            <div class="category-action edit" data-url="http://localhost/assessment-backend/categories/update?id=<?=$category->getId()?>">
              <span>Edit</span>
            </div>
            <div class="category-action delete" data-id="<?=$category->getId()?>"><span>Delete</span></div>
          </div>
        </td>
      </tr>
     <?php endforeach;?>
    </table>
  </main>


  <?php require_once "footer.php"?>
</body>
</html>
