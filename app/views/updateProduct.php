
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Add Product</title>
  <meta charset="utf-8">
 <?php require_once "header.php"?>
  <main class="content">
    <h1 class="title new-item">New Product</h1>

    <form id="update-product" enctype="multipart/form-data">
      <div class="input-field">
        <label for="id" class="label">Product Id</label>
        <input value="<?=$param['product']->getId()?>" type="text" id="id" class="input-text" name="id" readonly disabled/> 
      </div>
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input value="<?=$param['product']->getSku()?>" type="text" id="sku" class="input-text" name="sku"/> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input value="<?=$param['product']->getName()?>" type="text" id="name" class="input-text" name="name"/> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input value="<?=$param['product']->getPrice()?>" type="number" min="0" max="100000" step="0.01" id="price" class="input-text" name="price"/> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input value="<?=$param['product']->getQuantity()?>" type="number" min="0" max="100000" id="quantity" class="input-text" name="qty" /> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="categories" class="input-text" name="categories">
          <?php foreach ($param['categories'] as $category):?>
          <option 
            value="<?=$category->getId()?>"
            <?php if (in_array($category->getId(), $param['product']->getCategories())):?> selected <?php endif;?>
          ><?=$category->getName()?></option>
          <?php endforeach;?>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" class="input-text"><?=$param['product']->getDescription()?></textarea>
      </div>
      <div class="actions-form">
        <a href="http://localhost/assessment-backend/products/" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
      </div>

    </form>
  </main>
  <?php require_once "footer.php"?>
</body>
</html>
