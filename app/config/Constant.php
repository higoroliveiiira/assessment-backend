<?php
/**
 * Server path
 */
if (isset($_SERVER['HTTP_HOST'])) {
	define('APP_HOST', 	$_SERVER['HTTP_HOST']);
}
define('PATH_URL', 	'http://127.0.0.1/assessment-backend/app/');
define('PATH_URL_IMAGES',  PATH_URL.'assets/images');
define('PATH_URL_CSS',  PATH_URL.'assets/css');
define('PATH_URL_JS',  PATH_URL.'assets/js');
define('PATH'    , 	'app');

/**
 * DB
 */
define("DB_HOST"     , "127.0.0.1");
define("DB_DATABASE" , "webjump_db");
define("DB_LOGIN"    , "root");
define("DB_PASSWORD" , "root");
