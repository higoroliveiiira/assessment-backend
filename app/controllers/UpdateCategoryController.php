<?php

namespace App\controllers;

use App\services\CategoryService;

class UpdateCategoryController extends BaseController implements IController
{

	public function __construct()
    {
        $this->_service = new CategoryService();
    }

    public function process()
    {
        $requestData = [];
        parse_str(file_get_contents('php://input'), $requestData);
        return $this->_service->update($requestData);

    }

	public function index()
	{

        $categoryId = (int) $_GET['id'];
		$this->renderView("updateCategory", $this->_service->load($categoryId));
	}

}