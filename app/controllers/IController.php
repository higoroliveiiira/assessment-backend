<?php

namespace App\controllers;

interface IController
{
	public function process();
	public function index();
}