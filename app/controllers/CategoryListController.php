<?php

namespace App\controllers;

use App\services\CategoryService;

class CategoryListController extends BaseController implements IController
{

	public function __construct()
    {
        $this->_service = new CategoryService();
    }

    public function process(){}

	public function index()
	{

		$this->renderView("categories", $this->_service->getList());
	}

}