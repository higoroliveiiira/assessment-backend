<?php

namespace App\models;

use App\helpers\Logger;

class Category 
{
    private $__id;
    private $__name;

    public function __construct($data)
    {
        $this->__id = isset($data['id']) ? $data['id'] : NULL;
        $this->__name =  isset($data['name']) ? $data['name'] : NULL;
    }

    public function getId()
    {
        return $this->__id;
    }    

    public function getName()
    {
        return $this->__name;
    }  

    /**
     * Get Category List.
     * @param $connection  - Database connection.
    **/

    public static function getList(\PDO $connection)
    {

        $statement = $connection->prepare(
            "
            SELECT 
                *
            FROM
                category
            ORDER BY id DESC;
            "
        );
        $statement->execute();
        return $statement->fetchAll($connection::FETCH_ASSOC);
    }

    /**
     * Get Category Info.
     * @param $connection  - Database connection.
     * @param $categoryId  - Category id to load.
    **/
    public static function loadCategory(\PDO $connection, $categoryId)
    {
        try{
            $statement = $connection->prepare(
                "
                SELECT 
                    *
                FROM
                    category
                WHERE id = :id;
                "
            );
            $statement->bindValue(":id", $categoryId, \PDO::PARAM_INT);
            $statement->execute();
            $dataToLog = ['id' => $categoryId];
            Logger::write('Categoria de codigo %id% carregada com sucesso', $dataToLog);
            return $statement->fetch(\PDO::FETCH_ASSOC);
        }catch(PDOException $Exception){
            throw new $Exception->getMessage();
        }
    }

    /**
     * Insert Category in Database.
     * @param $connection  - Database connection.
     * @param $category  - Category to insert.
    **/
    public static function createCategory(\PDO $connection, Category $category)
    {
        try{

            $statement = $connection->prepare(
                "
                    INSERT INTO
                        category
                    (name)
                    VALUES
                    (:name)
                "
            );

            $statement->bindValue(":name", $category->getName());


            $statement->execute();
            $dataToLog = ['name' => $category->getName()];
            Logger::write('Categoria de nome %name% criada com sucesso', $dataToLog);
            return "Categoria criada com sucesso";
        }catch(PDOException $Exception){
            throw new $Exception->getMessage();
        }
    }

    /**
     * Update Category in Database.
     * @param $connection  - Database connection.
     * @param $category  - Category to update.
    **/
    public static function updateCategory(\PDO $connection, Category $category)
    {
        try{

            $statement = $connection->prepare(
                "
                    UPDATE
                        category
                    SET name = :name
                    WHERE
                        id = :id;
                "
            );

            $statement->bindValue(":name", $category->getName());
            $statement->bindValue(":id", $category->getId());

            $dataToLog = ['name' => $category->getName(), 'id' => $category->getId()];
            Logger::write('Categoria de nome %name% e id %id% atualizada com sucesso', $dataToLog);
            $statement->execute();
            return "Categoria atualizada com sucesso";
        }catch(PDOException $Exception){
            throw new $Exception->getMessage();
        }
    }

    /**
     * Delete Category in Database.
     * @param $connection  - Database connection.
     * @param $categoryId  - Category id to delete.
    **/
    public static function deleteCategory(\PDO $connection, $categoryId)
    {
        try{

            $statement = $connection->prepare(
                "
                DELETE FROM
                    category
                WHERE
                    id = :id
                "
            );

            $statement->bindValue(":id", (int) $categoryId, \PDO::PARAM_INT);
            $statement->execute();
            $dataToLog = ['id' => $categoryId];
            Logger::write('Categoria de id %id% excluída com sucesso', $dataToLog);
            return "Categoria excluída com sucesso";
        }catch(PDOException $Exception){
            throw new $Exception->getMessage();
        }
    }

}
