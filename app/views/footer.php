<footer>
	<div class="footer-image">
		<img src="<?= PATH_URL_IMAGES ?>/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
	</div>
	<div class="email-content">
		<span>go@jumpers.com.br</span>
	</div>
	<script type="text/javascript" src="<?=PATH_URL_JS?>/libs/jQuery/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="<?=PATH_URL_JS?>/site/product.js"></script>
	<script type="text/javascript" src="<?=PATH_URL_JS?>/site/category.js"></script>
</footer>