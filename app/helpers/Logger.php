<?php

namespace App\helpers;

class Logger
{
    /**
     * Write a logfile
     * @param $message  - Message to write in logfile.
     * @param $data - array of data to replace in message.
     */
    public static function write($message, array $data)
    {   
        foreach ($data as $key => $value) {
           $message = str_replace("%{$key}%", $value, $message);
        }

        $message .= PHP_EOL;

        file_put_contents(getcwd().'/app/assets/log/acoes.log', '['.date('d/m/Y').'] - '.$message, FILE_APPEND);
        
        
    }
}
