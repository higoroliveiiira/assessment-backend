<?php
define("FILENAME",  'app/assets/import.csv');
define("FILE_NOT_EXISTS",  'Arquivo inexistente');
define("ERROR",  'Arquivo inexistente');

require_once "vendor/autoload.php";
use App\config\Database;


function import(){
    if (!file_exists(FILENAME)){
        die(FILE_NOT_EXISTS);
    }

    $file = openFile(FILENAME);

    if(!$file){
        die (ERROR);
    }
    $i = 0;

    $dbConnection = Database::getConnection();

    if (!$dbConnection){
        die(mysqli_connect_errno());
    }

    $alreadyExists = [];
    try{
        while(($data = fgetcsv($file, 0, ';')) !== FALSE){
            if($i == 0){
                $i++;
                continue;
            }

            $queryProduct = "
                INSERT INTO
                    products
                (name, sku, description, price, quantity)
                VALUES
                (:name, :sku, :description, :price, :quantity)
            ";
            $statement = $dbConnection->prepare($queryProduct);
            $statement->bindValue(":name", $data[0]);
            $statement->bindValue(":sku", $data[1]);
            $statement->bindValue(":description", $data[2]);
            $statement->bindValue(":price", $data[3]);
            $statement->bindValue(":quantity", $data[4]);
            $statement->execute();
            $productId = $dbConnection->lastInsertId();
            $categories = explode('|', $data[5]);

            foreach ($categories as $category){
                if (!in_array($category, $alreadyExists)){
                    $queryCategory = "INSERT INTO category (name) VALUES (:name);";
                    $statementCategory = $dbConnection->prepare($queryCategory);
                    $statementCategory->bindValue(":name", $category);
                    $statementCategory->execute();
                    $alreadyExists[$dbConnection->lastInsertId()] = $category;
                }

                $queryCategoryHasProduct = "
                    INSERT INTO
                        category_has_product
                    (products_id, category_id)
                    VALUES 
                    (:products_id, :category_id);
                ";
                $statementCategoryHasProduct = $dbConnection->prepare($queryCategoryHasProduct);
                $statementCategoryHasProduct->bindValue(":products_id", $productId);
                $statementCategoryHasProduct->bindValue(":category_id", array_search($category, $alreadyExists));
                $statementCategoryHasProduct->execute();

            }
            echo '#';
        }
        echo "Importação realizada com sucesso";
    }catch(PDOException $Exception){
        throw new $Exception->getMessage();
    }
}

function openFile($fileName){
    return fopen($fileName, "r");
}

import();
