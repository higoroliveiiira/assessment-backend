<?php

namespace App\controllers;

use App\services\CategoryService;

class CreateCategoryController extends BaseController implements IController
{

	public function __construct()
    {
        $this->_service = new CategoryService();
    }

    public function process()
    {
    	$requestData = $_POST;
    	return $this->_service->create($requestData);
    }

	public function index()
	{

		$this->renderView("addCategory");
	}

}