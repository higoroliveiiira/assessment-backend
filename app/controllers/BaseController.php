<?php

namespace App\controllers;

class BaseController
{

    protected $_service;

    /**
     * Render a view.
     * @param $view  - Name of view file to render.
     * @param $param - Params to use in view.
     */
    public function renderView($view, $param = null)
    {

        require_once 'app/views/' . $view . '.php';
    }

}
