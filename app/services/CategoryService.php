<?php

namespace App\services;

use App\config\Database;
use App\models\Category;

class CategoryService implements IService
{
    private $___dbConnection = NULL;

    public function __construct()
    {
        $this->__dbConnection = Database::getConnection();
    }

    public function getList()
    {
        $categoriesInDb = Category::getList($this->__dbConnection);
        $categoriesList = [];

        foreach($categoriesInDb as $category)
        {   
            array_push($categoriesList, $this->factory($category));
        }

        return $categoriesList;
    }

    public function load($categoryId)
    {
        return $this->factory(Category::loadCategory($this->__dbConnection, $categoryId));
    }

    public function create($categoryData)
    {
        $category = $this->factory($categoryData);

        return Category::createCategory($this->__dbConnection, $category);
    }

    public function update($categoryData)
    {
        $category = $this->factory($categoryData);

        return Category::updateCategory($this->__dbConnection, $category);
    }

    public function delete($id)
    {
        return Category::deleteCategory($this->__dbConnection, $id);
    }

    public function factory($categoryData)
    {
        return new Category($categoryData);
    }
}
