<?php

namespace App\config;

class Database
{
    private static $__connection = NULL;
    
    public static function getConnection() 
    {
        if(!isset(self::$__connection)){
            self::$__connection = new \PDO(
                "mysql:host=".DB_HOST."; dbname=".DB_DATABASE.";charset=utf8", DB_LOGIN, DB_PASSWORD
            );
        }

        return self::$__connection;
    }
}
