<?php

namespace App\controllers;

use App\services\ProductService;
use App\services\CategoryService;

class DeleteProductController extends BaseController implements IController
{

    public function __construct()
    {
        $this->_service = new ProductService();
    }

    public function process()
    {   
        $requestData = [];
        parse_str(file_get_contents('php://input'), $requestData);

        return $this->_service->delete($requestData['id']);
    }

    public function index()
    {
        
    }

}