$("#create-category").submit(function(){
	createCategory()
	return false
})

$("#update-category").submit(function(){
	updateCategory()
	return false
})

$(".category-action.edit").click(function(){
	window.location = this.dataset.url
})

$(".category-action.delete").click(function(){
	if (confirm("Tem certeza que deseja deletar a categoria?")){
		deleteCategory(this.dataset.id)
	}
	return false
})

function createCategory(){
	const data = {
		name: $("#category-name").val(),
	}

	validationErrors = validateAttributes(data)
	if (validationErrors.length === 0){
		$.ajax({
			url: "http://localhost/assessment-backend/categories/create",
			data: data,
			method:"POST",
			success: function (response){
				let errorElement = $("<p></p>", {
					text: "Categoria Adicionada com sucesso",
					class: 'success'
				}).appendTo("#create-category")
			}
		})
		$("#category-name").val("")
	}

	showErrors(validationErrors)

}

function deleteCategory(id){
	const data = {id: id}
	$.ajax({
		url: "http://localhost/assessment-backend/categories/delete",
		data: data,
		method:"DELETE",
		success: function (response){
			let errorElement = $("<p></p>", {
				text: "Categoria Excluída com sucesso",
				class: 'success'
			}).prependTo(".data-grid")
			setTimeout(function(){
				location.reload()
			}, 500)
		}
	})
}
function updateCategory(){
	const data = {
		id: $("#category-id").val(),
		name: $("#category-name").val()	
	}

	validationErrors = validateAttributes(data)
	if (validationErrors.length === 0){
		$.ajax({
			url: `http://localhost/assessment-backend/categories/updateAction`,
			data: data,
			method: "PUT",
			success: function (response){
				let errorElement = $("<p></p>", {
					text: "Categoria Atualizada com sucesso",
					class: 'success'
				}).appendTo("#update-category")
				setTimeout(function(){
					location.reload()
				}, 1000)
			}
		})
	}

	showErrors(validationErrors)

}

function showErrors(errors){

	errors.forEach(function(e){
		let errorElement = $("<p></p>", {
			text: e,
			class: 'error'
		}).appendTo("#create-product")
	})

}


function validateAttributes(data){
	let errors = []

	if (data.name === null || data.name.length === 0){
		errors.push("Nome da categoria deve ser preenchido")
	}

	return errors
}