<?php 

$array_routes['/assessment-backend/products/'] = array(         // Route
    "controller" => "App\controllers\ProductListController", // Controller 
    "action"     => "index"                           // Function 
);

$array_routes['/assessment-backend/products/new'] = array(         // Route
    "controller" => "App\controllers\CreateProductController", // Controller 
    "action"     => "index"                           // Function 
);

$array_routes['/assessment-backend/products/create'] = array(         // Route
    "controller" => "App\controllers\CreateProductController", // Controller 
    "action"     => "process"                           // Function 
);

$array_routes['/assessment-backend/products/update'] = array(         // Route
    "controller" => "App\controllers\UpdateProductController", // Controller 
    "action"     => "index"                           // Function 
);

$array_routes['/assessment-backend/products/updateAction'] = array(         // Route
    "controller" => "App\controllers\UpdateProductController", // Controller 
    "action"     => "process"                           // Function 
);

$array_routes['/assessment-backend/products/delete'] = array(         // Route
    "controller" => "App\controllers\DeleteProductController", // Controller 
    "action"     => "process"                           // Function 
);
return $array_routes;